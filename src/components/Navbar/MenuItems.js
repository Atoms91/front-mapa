export const MenuItems = [
    {
        title: 'Mapa',
        url: '/',
        cName: 'nav-links'
    },
    {
        title: 'Contacta',
        url: 'Contacta',
        cName: 'nav-links'
    },
    {
        title: 'Proyecto',
        url: 'Proyecto',
        cName: 'nav-links'
    },
    {
        title: 'Nosotros',
        url: 'Nosotros',
        cName: 'nav-links'
    },
    {
        title: 'Crear Evento',
        url: 'crearEvento',
        cName: 'nav-links'
    },
    {
        title: 'Registrate',
        url: 'login',
        cName: 'nav-links-mobile'
    }

]